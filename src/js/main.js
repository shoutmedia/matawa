import GoogleMap from './gmaps';
import SmoothScroll from './smoothscroll';
import { slide } from './slide';

/**
 * Activate smooth scroll
 */
const scroll = new SmoothScroll();

/**
 * Google maps
 */
const maps = document.querySelectorAll('.acf-map');

Array.from(maps).forEach((map) => {
    const markers = map.querySelectorAll('.marker');
    const gmap = new GoogleMap(map);

    Array.from(markers).forEach((marker) => {
        gmap.addMarker(marker);
    });

    gmap.centerMap();
});

/**
 * Add anchor link tracking
 */
const links = document.querySelectorAll('a[href*="#"]');

Array.from(links).forEach((element) => {
    let href = element.getAttribute('href');

    // Clean up URL
    if (href.indexOf('#') !== false) {
        href = href.substr(href.indexOf('#'));
    }

    if (href.length > 2) {
        element.addEventListener('click', (e) => {
            // Make sure the anchor is on the current page
            if (location.pathname.replace(/^\//, '') === element.pathname.replace(/^\//, '')
                && location.hostname === element.hostname) {
                e.preventDefault();
                scroll.to(href);
            }
        });
    }
});

window.addEventListener('load', () => {
    if (window.location.hash) {
        scroll.to(window.location.hash);
    }
});

const backgroundWrap = document.querySelectorAll('.background-wrap');

Array.from(backgroundWrap).forEach((element) => {
    const words = element.innerHTML.split(' ');
    element.innerHTML = `<span>${words.join('</span> <span>')}</span>`;
});

const searchForm = document.querySelector('#searchform');
const searchToggle = document.querySelector('.search-toggle');
const searchButton = searchForm.querySelector('.btn-search');
const searchField = searchForm.querySelector('.search-field');
const searchClose = searchForm.querySelector('.btn-close');

searchToggle.addEventListener('click', () => {
    if (searchForm.classList.contains('open')) {
        searchForm.classList.remove('open');
        searchToggle.classList.remove('open');
    } else {
        searchForm.classList.add('open');
        searchField.focus();
        searchToggle.classList.add('open');
    }
});

searchField.addEventListener('blur', () => {
    if (searchField.value === '') {
        searchForm.classList.remove('open');
        searchToggle.classList.remove('open');
    }
});

searchClose.addEventListener('click', (e) => {
    e.preventDefault();
    searchField.value = '';
    searchForm.classList.remove('open');
    searchToggle.classList.remove('open');
});

searchButton.addEventListener('click', (e) => {
    if (searchField.value === '') {
        e.preventDefault();
    }
});

const accordions = document.querySelectorAll('.accordion-content');

Array.from(accordions).forEach((accordion) => {
    const items = accordion.querySelectorAll('.item-heading');

    Array.from(items).forEach((item) => {
        item.addEventListener('click', () => {
            const content = item.nextElementSibling;

            if (item.classList.contains('active')) {
                slide(content, 300, 'up', 'ease-in-out');
                item.classList.remove('active');
            } else {
                slide(content, 300, 'down', 'ease-in-out');
                item.classList.add('active');
            }
        });
    });
});

const navToggle = document.querySelectorAll('.mobile-nav-toggle');
const mobileNav = document.querySelector('.mobile-nav');
const body = document.querySelector('body');

Array.from(navToggle).forEach((button) => {
    button.addEventListener('click', (e) => {
        e.stopPropagation();

        if (mobileNav.classList.contains('open')) {
            mobileNav.classList.remove('open');
        } else {
            mobileNav.classList.add('open');
        }
    });
});

body.addEventListener('click', (e) => {
    if (mobileNav.classList.contains('open') && !e.target.classList.contains('search-field')) {
        mobileNav.classList.remove('open');
    }
});

const mainNavs = document.querySelectorAll('.menu-item-has-children');

Array.from(mainNavs).forEach((navItem) => {
    const subnav = navItem.querySelector('.sub-menu');

    if (subnav) {
        navItem.addEventListener('mouseenter', () => {
            subnav.classList.add('open');
        });

        navItem.addEventListener('mouseleave', () => {
            subnav.classList.remove('open');
        });
    }
});

jQuery(($) => {
    $('.slideshow .slides').slick({
        autoplay: true,
        autoplaySpeed: 6000,
        arrows: false,
        dots: true,
        draggable: false,
        slide: '.slide',
    });
});
