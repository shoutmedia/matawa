const fs = require('fs');

function patchOtf2ttf() {
    fs.readFile('node_modules/otf2ttf/otf2ttf.sh', 'utf-8', (readError, data) => {
        if (readError) {
            throw readError;
        }

        let output = data;

        // Replace 2 offending lines
        output = output.replace('\nPrint(\'    "fontSourceFile":    "\' + $1 + \'",\');', '\n#Print(\'    "fontSourceFile":    "\' + $1 + \'",\');');
        output = output.replace('\nPrint(\'    "fontPath": "\' + $2 + \'",\');', '\n#Print(\'    "fontPath": "\' + $2 + \'",\');');

        // Remove comma from Line 10
        output = output.replace('\nPrint(\'    "fontFile": "\' + fontFile + \'",\');', '\nPrint(\'    "fontFile": "\' + fontFile + \'"\');');

        // Comment out Copyright
        output = output.replace('\nPrint(\'    "copyright":   "\' + $copyright + \'"\');', '\n#Print(\'    "copyright":   "\' + $copyright + \'"\');');

        fs.writeFile('node_modules/otf2ttf/otf2ttf.sh', output, 'utf-8', (writeError) => {
            if (writeError) {
                throw writeError;
            }

            console.log('otf2ttf patched');
        });
    });

    return true;
}

patchOtf2ttf();
