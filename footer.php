<footer class="footer" role="contentinfo">
    <div class="container">
        <div class="logo">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.png" alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>">
        </div>

        <div class="iso-logo">
            <?php foreach (get_field('footer_logos', 'option') as $logos) :

                if ($logos['logo_link']) : ?>
                    <a href="<?php echo $logos['logo_link']; ?>">
                        <img src="<?php echo $logos['logo']['url']; ?>" alt="<?php echo $logos['logo']['alt']; ?>">
                    </a>
                <?php else : ?>
                    <img src="<?php echo $logos['logo']['url']; ?>" alt="<?php echo $logos['logo']['alt']; ?>">
                <?php endif; ?>
            <?php endforeach; ?>
        </div>

        <div class="inner">
            <nav class="nav" aria-label="site">
                <?php wp_nav_menu([
                    'theme_location' => 'footer',
                    'depth' => 1,
                ]); ?>
            </nav>

            <div class="contact">
                <?php if (get_field('phone_numbers', 'option')) : ?>
                    <?php foreach (get_field('phone_numbers', 'option') as $phone) : ?>
                        <div class="phone">
                            <span class="label"><?php _e($phone['label'], DOMAIN); ?>:</span>
                            <?php if ($phone['type'] !== 'faxNumber') : ?>
                                <a href="tel:<?php echo preg_replace('/[^0-9,.]/', '', $phone['number']); ?>">
                                <?php endif; ?>
                                <span itemprop="<?php echo $phone['type'] == 'faxNumber' ? $phone['type'] : 'telephone'; ?>"><?php echo $phone['number']; ?></span>
                                <?php if ($phone['type'] !== 'faxNumber') : ?>
                                </a>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>

                <a href="<?php echo get_field('footer_button_link', 'option'); ?>" class="btn-yellow">
                    <?php echo get_field('footer_button_label', 'option'); ?>
                    <i class="fas fa-caret-square-right" aria-hidden="true"></i>
                </a>

            </div>

        </div>

    </div>
</footer>

<div id="footer-credit" class="<?php the_field('site_credit_style', 'option'); ?>"></div>

<?php wp_footer(); ?>

</body>

</html>