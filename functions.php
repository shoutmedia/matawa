<?php

// Development Environment (local, staging or production)
define('ENV', 'local');

// Translation string domain
define('DOMAIN', 'arcadia');

// Current directory
define('BASE', dirname(__FILE__));

// Google Maps API Key
define('GOOGLEAPI', 'AIzaSyDivzhX9fUCCqvaJWhbVXu-y1EDBCycwuU');

// Helper functions
require_once(BASE . '/inc/helpers.php');

// Autoloader
require_once(BASE . '/inc/autoloader.php');

// Load Arcadia
\Arcadia\Setup::init();

/**
 * Current Theme Setup
 */
function theme_setup()
{
    // Thumbnails
    add_theme_support('post-thumbnails');

    // Title Tag
    add_theme_support('title-tag');

    // Menus
    register_nav_menu('primary', __('Navigation Menu', DOMAIN));
    register_nav_menu('footer', __('Footer Menu', DOMAIN));
    register_nav_menu('mobile', __('Mobile Menu', DOMAIN));
    register_nav_menu('secondary', __('Secondary Menu', DOMAIN));

    // When inserting an image don't link it
    update_option('image_default_link_type', 'none');

    // Remove Gallery Styling
    add_filter('use_default_gallery_style', '__return_false');

    // Additional Image Sizes
    add_image_size('banner', 1920, 1920);

    // Add staff post type
    Arcadia\Theme\PostTypes::genericPostType('staff', 'Staff', 'Staff', 55, [
        'supports' => [
            'title',
            'revisions',
            'thumbnail',
        ],
        'taxonomies' => [
            'groups',
        ],
    ]);

    register_taxonomy('groups', 'staff', [
        'labels' => __('Groups', DOMAIN),
        'public' => true,
        'hierarchical' => true,
    ]);
}

add_action('after_setup_theme', 'theme_setup');

add_filter('manage_staff_posts_columns', function ($columns) {
    unset($columns['date']);

    $columns['groups'] = __('Groups', DOMAIN);
    $columns['date'] = __('Date', DOMAIN);

    return $columns;
}, 10, 2);

add_action('manage_staff_posts_custom_column', function ($column, $post_id) {
    switch ($column) {
        case 'groups':
            $terms = get_the_terms($post_id, 'groups');

            if ($terms) {
                echo implode(',', array_map(function ($term) {
                    return $term->name;
                }, $terms));
            }

            break;
    }
}, 10, 2);

/**
 * Load theme specific assets
 */
function scripts_styles()
{
    // Load Stylesheets
    wp_enqueue_style(DOMAIN . '-slick', get_template_directory_uri() . '/slick/slick.css');
    wp_enqueue_style(DOMAIN . '-theme', get_template_directory_uri() . '/slick/slick-theme.css');
    wp_enqueue_style(DOMAIN . '-style', get_stylesheet_uri());

    // Google Maps
    wp_register_script(DOMAIN . '-gmaps', 'https://maps.googleapis.com/maps/api/js?key=' . GOOGLEAPI, [], null, true);

    if (Layout::containsBlock('map')) {
        wp_enqueue_script(DOMAIN . '-gmaps');
    }

    // Load JavaScript
    wp_enqueue_script(DOMAIN . '-polyfill', 'https://cdn.polyfill.io/v2/polyfill.min.js', [], null);
    wp_enqueue_script(DOMAIN . '-slick', get_template_directory_uri() . '/slick/slick.min.js', [], null, true);
    wp_enqueue_script(DOMAIN . '-main', get_template_directory_uri() . '/js/main.js', [], null, true);
}

add_action('wp_enqueue_scripts', 'scripts_styles');

/**
 * Custom Editor Styles
 * @param  array $init TinyMCE Options
 * @return array       Updated TinyMCE Options
 */
function editor_style_options($init)
{
    $style_formats = [
        [
            'title' => 'Small Text',
            'selector' => '*',
            'classes' => 'text-small',
        ],
        [
            'title' => 'Medium Text',
            'selector' => '*',
            'classes' => 'text-medium',
        ],
        [
            'title' => 'Large Text',
            'selector' => '*',
            'classes' => 'text-large',
        ],
        [
            'title' => 'Extra Large Text',
            'selector' => '*',
            'classes' => 'text-xlarge',
        ],
        [
            'title' => 'Line Height None',
            'selector' => '*',
            'classes' => 'line-none',
        ],
        [
            'title' => 'Line Height Small',
            'selector' => '*',
            'classes' => 'line-small',
        ],
        [
            'title' => 'Line Height Normal',
            'selector' => '*',
            'classes' => 'line-normal',
        ],
        [
            'title' => 'Line Height Medium',
            'selector' => '*',
            'classes' => 'line-medium',
        ],
        [
            'title' => 'Line Height Large',
            'selector' => '*',
            'classes' => 'line-large',
        ],
        [
            'title' => 'Button',
            'block' => 'a',
            'classes' => 'btn',
        ],
    ];

    $init['style_formats'] = json_encode($style_formats);

    return $init;
}

add_filter('tiny_mce_before_init', 'editor_style_options');

/**
 * ACF Maps Key
 */
function my_acf_init()
{
    acf_update_setting('google_api_key', GOOGLEAPI);
}

add_action('acf/init', 'my_acf_init');

/**
 * Colour Palette
 */
function set_acf_rgba_color_picker_palette()
{
    $palette = array(
        'rgba(238, 49, 36, 1)',
        'rgba(255, 210, 0, 1)',
        'rgba(101, 87, 77, 1)',
        'rgba(88, 108, 48, 1)',
        'rgba(143, 145, 148, 1)',
        'rgba(35, 31, 32, 1)',
        'rgba(0,0,0, 1)',
        'rgba(255,255,255, 1)'
    );

    return $palette;
}

add_filter('acf/rgba_color_picker/palette', 'set_acf_rgba_color_picker_palette');

/**
 * add banner to simple job board
 */
add_action('sjb_before_main_content', function () {
    Banner::render(['type' => 'page']);
});

/**
* add layout to simple job board
*/
add_action('sjb_after_main_content', function () {
    Layout::render(['default' => '']);
});

/*
* add siteurl to the prepend of the relative url link field
*/
function my_acf_load_field($field)
{
    $field['prepend'] = site_url('/');
    return $field;
}

add_filter('acf/load_field/name=relative_url', 'my_acf_load_field');

/**
 * shortcodes
 */
function get_theme_setting($atts)
{
    $a = shortcode_atts(array(
      'field' => '',
      'number' => '',
    ), $atts);

    if ($atts['field'] === 'phone_number') {
        return get_field('phone_numbers', 'option')[$atts['number']]['number'];
    }

    if ($atts['field'] === 'phone_numbers') {
        $html = '';
        foreach (get_field('phone_numbers', 'option') as $phone) {
            $html .= '<div class="phone">';
            $html .= '<span class="label">' . __($phone['label'], DOMAIN) . ': </span>';
            if ($phone['type'] !== 'faxNumber') {
                $html .= '<a href="tel:' . preg_replace('/[^0-9,.]/', '', $phone['number']). '">';
            }
            $html .= '<span>' . $phone['number'] . '</span>';
            if ($phone['type'] !== 'faxNumber') {
                $html .= '</a>';
            }
            $html .= '</div>';
        }
        return $html;
    }

    if ($atts['field'] === 'address') {
        return get_field('address', 'option') . ',<br />' . get_field('city', 'option') . ', ' . get_field('province', 'option') . ' ' . get_field('postal_code', 'option');
    }

    if ($atts['field'] === 'hours') {
        return OpeningHours::get(['combine' => 'linear']);
    }

    if ($atts['field'] === 'social') {
        $html = '';

        foreach (get_field('social_platforms', 'option') as $option) {
            $html .= '<a href="' . $option['url'] . '" target="_blank" rel="noopener">' . $option['label'] . '</em></a><br />';
        }

        return $html;
    }

    return get_field($a['field'], 'option');
}
add_shortcode('themesetting', 'get_theme_setting');
