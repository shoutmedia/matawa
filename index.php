<?php get_header(); ?>

    <main class="main-content" role="main" id="main" tabindex="-1">

        <?php Banner::render([
            'type' => 'blog',
        ]); ?>

        <?php Layout::render([
            'default' => 'blog_index',
            'type' => 'blog',
        ]); ?>

    </main>

<?php get_footer(); ?>
