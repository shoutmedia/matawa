<div class="<?php Layout::classes('banner banner-default'); ?>" style="<?php Layout::partial('background'); ?>">
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" class="logo">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.png" alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>">
        </a>
        <div class="title-container">
            <img src="<?php echo get_template_directory_uri(); ?>/img/ojibway-tagline-white.png" alt="The Power of Unity. The Dignity of Difference.">
            <?php if (Field::exists('banner_title')) : ?>
                <h1 class="banner-title"><?php Field::display('banner_title'); ?></h1>
            <?php endif; ?>
        </div>
    </div>
</div>
