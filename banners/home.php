<div class="<?php Layout::classes('banner banner-home'); ?>" style="<?php Layout::partial('background'); ?>">
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <div class="inner">
            <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home" class="logo">
                <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>">
            </a>
            <?php if (Field::exists('banner_title')) : ?>
                <h1 class="title"><?php Field::display('banner_title'); ?></h1>
            <?php endif; ?>
            <?php Field::display('banner_desc') ?>
            <div class="tagline">
                <img src="<?php echo get_template_directory_uri(); ?>/img/ojibway-tagline.png" alt="The Power of Unity. The Dignity of Difference.">
            </div>
        </div>
    </div>
    <img class="accent-image" src="<?php echo get_template_directory_uri(); ?>/img/home-banner-accent.png">
</div>
<div class="container">
    <div class="featured-links">

        <?php foreach (Field::iterable('featured_links') as $link):

                $label =  Field::get('btn_label');

                if (Field::exists('add_btn_icon')) :
                    $label = Field::get('btn_icon') . Field::get('btn_label');
                endif;

                ?>
                <a href="<?php Layout::partial('link'); ?>"<?php Field::displayIfEquals('btn_target', '_blank', ' target="%s"'); ?> class="featured-link<?php Field::html('btn_icon_placement', ' icon-%s'); Field::html('size', ' %s');?>" style="background-image: url(<?php echo Field::src('background_image', 'full'); ?>)">
                    <div class="link-title">
                        <?php if (Field::get('size') === 'large') _e('Featured') ; ?>
                        <h3 class="background-wrap"><?php Field::display('title'); ?></h3>
                    </div>
                    <div class="link-label"><?php echo $label; ?></div>
                </a>

        <?php endforeach; ?>

    </div>
</div>
