<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php

    if (is_search()) {
        echo '<meta name="robots" content="noindex, nofollow">' . PHP_EOL;
    }

    ?>
    <meta name="title" content="<?php wp_title('|', true, 'right'); ?>">
    <meta name="Copyright" content="Copyright <?php bloginfo('name'); ?> <?php echo date('Y'); ?>. All Rights Reserved.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php

    if (get_field('mobile_theme_colour', 'option')) {
        echo '<meta name="theme-color" content="' . get_field('mobile_theme_colour', 'option') . '">' . PHP_EOL;
    }

    if (get_field('ios_status_bar', 'option')) {
        echo '<meta name="apple-mobile-web-app-status-bar-style" content="' . get_field('ios_status_bar', 'option') . '">' . PHP_EOL;
    }

    ?>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <?php wp_head(); ?>
    <?php Arcadia\Theme\Assets::analyticsTrackingCode(); ?>
</head>

<body <?php body_class(); ?>>
    <a href="#main" class="skip-link"><?php _e('Skip to content', DOMAIN); ?></a>

    <div class="mobile-nav">
        <div class="mobile-nav-top">
            <form role="search" class="mobile-search" method="get" action="<?php echo home_url('/'); ?>">
                <input class="search-field" type="text" id="s" name="s" value="" placeholder="<?php _e('Search', DOMAIN); ?>"/>

                <button type="submit" class="btn-search"><i class="fa fa-search" aria-hidden="true"></i></button>
            </form>
            <div class="close-btn" aria-label="close">
                <i class="fas fa-times"></i>
            </div>
        </div>
        <nav class="navigation" role="navigation" aria-label="site">
            <?php wp_nav_menu([
                'theme_location' => 'primary',
                'depth' => 1,
            ]); ?>
        </nav>
        <nav class="secondary"  aria-label="site">
            <?php wp_nav_menu([
                'theme_location' => 'secondary',
            ]); ?>
        </nav>
        <div class="phone-number">
            <?php if (get_field('phone_numbers', 'option')) : ?>
                <?php foreach (get_field('phone_numbers', 'option') as $phone) : ?>
                    <div class="phone-container">
                        <?php _e($phone['label'], DOMAIN); ?> | <span itemprop="<?php echo $phone['type'] == 'faxNumber' ? $phone['type'] : 'telephone'; ?>">
                            <?php if ($phone['type'] !== 'faxNumber') : ?>
                            <a href="tel:<?php echo $phone['number']; ?>">
                            <?php endif; ?>
                                <?php echo $phone['number']; ?>
                            <?php if ($phone['type'] !== 'faxNumber') : ?>
                            </a>
                            <?php endif; ?>
                        </span>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>

        <div class="email"><a href="mailto:<?php echo get_field('email', 'option'); ?>"><?php echo get_field('email', 'option'); ?></a></div>
    </div>

    <div class="topbar">
        <div class="container">
            <?php Layout::partial('social', ['start' => '<div class="social">', 'end' => '</div>']); ?>
            <nav class="secondary"  aria-label="site">
                <?php wp_nav_menu([
                    'theme_location' => 'secondary',
                    'depth' => 1,
                ]); ?>
            </nav>
        </div>
    </div>

    <header class="header" role="banner">
        <div class="container">
            <div class="mobile-nav-toggle" aria-label="mobile menu">
                <i class="fas fa-bars"></i>
                <?php _e('Menu', DOMAIN); ?>
            </div>
            <nav class="navigation" role="navigation" aria-label="site">
                <?php wp_nav_menu([
                    'theme_location' => 'primary',
                ]); ?>
            </nav>
            <?php include 'searchform.php'; ?>
        </div>
    </header>
