<aside class="sidebar">

    <h2><?php _e('Archives', DOMAIN); ?></h2>
    <ul>
        <?php wp_get_archives('type=monthly'); ?>
    </ul>

</aside>
