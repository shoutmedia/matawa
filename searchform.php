<div class="search-container">
    <form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>">
        <label for="s" class="screen-reader-text"><?php _e('Search for:', DOMAIN); ?></label>
        <input class="search-field" type="text" id="s" name="s" value="" placeholder="<?php _e('Search', DOMAIN); ?>"/>

        <button type="submit" class="btn-search"><i class="fa fa-search" aria-hidden="true"></i></button>
        <button class="btn-close"><i class="fa fa-times" aria-hidden="true"></i></button>
    </form>
    <button class="search-toggle"><i class="fa fa-search" aria-hidden="true"></i></button>
</div>
