<?php
/**
 * Schema Output
 */
class Schema
{
    /**
     * Return Schema openingHours
     * @param  array $opts Rendering options
     * @return void
     */
    public static function openingHours($opts = [])
    {
        OpeningHours::render($opts);
    }
}
