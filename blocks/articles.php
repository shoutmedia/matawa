<div class="<?php Layout::classes('articles'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <?php Layout::partial('title'); ?>
        <div class="articles-list">
            <?php foreach(Field::get('articles') as $loop): ?>
                <div class="article">
                    <h4 class=""><?php echo $loop->post_title; ?></h4>
                    <div class="read-more">
                        <a href="<?php echo get_the_permalink($loop->ID); ?>">Read Article</a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
