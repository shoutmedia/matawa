<div class="<?php Layout::classes('accent-image'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <div class="accent">
            <?php Field::image('accent', [465, 465]); ?>
        </div>
        <div class="content">
            <?php Layout::partial('title'); ?>
            <?php Field::display('content'); ?>
            <?php Layout::partial('buttons'); ?>
        </div>
    </div>
</div>
