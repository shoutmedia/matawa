<?php if (Field::exists('title')) : ?>
    <div class="title-block <?php Field::display('title_alignment'); ?>">
        <?php Field::html('title', '<h2 class="title">%s</h2>'); ?>
    </div>
<?php endif; ?>
