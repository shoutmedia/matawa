<div class="title-container">
    <?php Layout::partial('title'); ?>
</div>

<?php if (Field::exists('items')) : ?>
    <div class="accordion-content">
        <?php foreach (Field::iterable('items') as $loop) : ?>
            <div class="item-heading">
                <span class="label"><?php Field::display('tab_heading'); ?></span>
                <i class="fas fa-chevron-down"></i>
            </div>
            <div class="item-content item-hidden"><?php Field::display('tab_content'); ?></div>

        <?php endforeach; ?>
    </div>
<?php endif; ?>
