<?php

if (Field::anyExist('link_resource', 'link_url', 'link_anchor', 'link_file')) :
    switch (Field::get('link_type')) :
        case 'internal':
            $url = Field::get('link_resource');
            break;
        case 'external':
            $url = Field::url('link_url');
            break;
        case 'relative':
            $url = site_url(Field::get('relative_url'));
            break;
        case 'anchor':
            $url = '#' . Field::get('link_anchor');
            break;
        case 'media':
            $url = Field::get('link_file');
            break;
    endswitch;

    echo $url;
endif;
