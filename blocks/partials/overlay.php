<?php if (!Field::equals('overlay_opacity', 0) && Field::allExist('overlay_opacity', 'overlay_colour')) : ?>
    <div class="overlay" style="background-color: <?php Field::display('overlay_colour'); ?>; opacity: <?php echo Field::get('overlay_opacity') / 100; ?>;"></div>
<?php endif; ?>
