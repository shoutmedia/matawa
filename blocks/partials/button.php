<?php

if (Field::exists('btn_label')) :
    $label = '<span class="btn-label">' . Field::get('btn_label') . '</span>';

    if (Field::exists('add_btn_icon')) :
        $label = Field::get('btn_icon') . $label;
    endif;

    ?>
    <a href="<?php Layout::partial('link'); ?>"<?php Field::displayIfEquals('btn_target', '_blank', ' target="%s"'); ?> class="btn<?php Field::html('btn_icon_placement', ' icon-%s'); ?>"><?php echo $label; ?></a>
<?php endif; ?>
