<?php

$styles  = [];

if (Field::exists('bg_colour')) {
    $styles[] = 'background-color: ' . Field::get('bg_colour');
}

if (Field::exists('bg_image')) {
    $styles[] = 'background-image: url(' . Field::src('bg_image', 'banner') . ')';
}

if (!Field::exists('class_bg_position')) {
    $styles[] = 'background-position: ' . Field::get('bg_position_x') . '% ' . Field::get('bg_position_y') . '%';
}

if ($styles) {
    echo implode(";", $styles);
}
