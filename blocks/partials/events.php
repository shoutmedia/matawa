<div class="events-content">
    <a href="<?php Field::display('events_page'); ?>" class="view-all"><?php _e('View All', DOMAIN); ?></a>
    <div class="events-list">
        <?php echo do_shortcode('[events_list scope="future" limit="' . Field::get('event_count') . '" ]' .
        '<div class="event">
            <a href="#_EVENTURL"><h4 class="background-wrap">#_EVENTNAME</h4>
            <div class="date">#_EVENTDATES</div>
            <div class="time">#_EVENTTIMES</div></a>
        </div>' .
        '[/events_list]'); ?>
    </div>
</div>
