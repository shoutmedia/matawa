<?php

$col_size = 12 / count(Field::get('columns', ['']));

?>
<div class="<?php Layout::classes('column-block'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <?php Layout::partial('title'); ?>
        <?php if (Field::exists('columns')) : ?>
            <div class="row">
                <?php foreach (Field::iterable('columns') as $loop) : ?>
                    <div class="col w<?php Field::display('size', $col_size); ?>">
                        <?php Field::html('title', '<h3 class="title">%s</h3>'); ?>
                        <?php Field::display('content'); ?>
                        <?php Layout::partial('buttons'); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
