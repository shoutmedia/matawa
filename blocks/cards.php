<?php

$per_row = Field::get('cards_per_row', 3);

?>
<div class="<?php Layout::classes('cards'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <?php Layout::partial('title'); ?>
        <?php if (Field::exists('cards')) : ?>
            <div class="items x<?php echo $per_row; ?>">
                <?php foreach (Field::iterable('cards') as $loop) : ?>
                    <div class="item">
                        <?php if (Field::anyExist('link_resource', 'link_url', 'link_anchor', 'link_file')) : ?>
                        <a href="<?php Layout::partial('link'); ?>" class="card-link">
                        <?php endif; ?>
                            <div class="item-thumbnail" style="background-image:url(<?php echo Field::src('image', 'medium'); ?>);"></div>
                            <div class="item-lower">
                                <?php Field::html('title', '<p class="label">%s</p>'); ?>
                                <?php if (Field::anyExist('link_resource', 'link_url', 'link_anchor', 'link_file')) : ?>
                                <div class="read-more"><?php _e('Read More', DOMAIN); ?></div>
                                <?php endif; ?>
                            </div>
                        <?php if (Field::anyExist('link_resource', 'link_url', 'link_anchor', 'link_file')) : ?>
                        </a>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
