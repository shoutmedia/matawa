<div class="twitter-follow-link">
    <p class="text-medium" style="text-align: center;"><a target="_blank" href="https://twitter.com/<?php Field::display('twitter_name'); ?>" class="twitter-link"><strong><?php _e('Follow us on Twitter', DOMAIN); ?> <span class="text-medium">@<?php Field::display('twitter_name'); ?></span></strong></a></p>
</div>
<div class="sidebar-twitter-feed">
    <div class="sidebar-tweet">
        <?php echo do_shortcode('[custom-twitter-feeds num=' . Field::get('number_of_tweets') . ']'); ?>
    </div>
    <div class="bottom">
        <div class="left"></div>
        <div class="right"></div>
    </div>
</div>
