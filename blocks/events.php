<div class="<?php Layout::classes('events'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <?php Layout::partial('title'); ?>
        <?php Layout::partial('events'); ?>
    </div>
</div>
