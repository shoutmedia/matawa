<?php

$per_row = Field::get('items_per_row', 3);

?>
<div class="<?php Layout::classes('content-grid'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <?php if (Field::exists('items')) : ?>
            <div class="items">
                <?php foreach (Field::iterable('items') as $loop) :

                    if ($loop->index % $per_row == 0 && $loop->index > 0) :
                        echo '</div><div class="items">';
                    endif;

                    ?>
                    <div class="item">
                        <?php Field::image('image', 'medium', ['class' => 'thumbnail']); ?>
                        <?php Field::html('label', '<p class="label">%s</p>'); ?>
                        <?php Field::display('description'); ?>
                        <?php Layout::partial('button'); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
