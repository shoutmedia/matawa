<?php

$per_row = Field::get('links_per_row', 3);

?>
<div class="<?php Layout::classes('link-grid'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <?php Layout::partial('title'); ?>
        <?php if (Field::exists('links')) : ?>
            <div class="items">
                <?php foreach (Field::iterable('links') as $loop) :
                    if ($loop->index % $per_row == 0 && $loop->index > 0) :
                        echo '</div><div class="items">';
                    endif;

                    ?>
                    <div class="item">
                        <?php if (Field::anyExist('link_resource', 'link_url', 'link_anchor', 'link_file')) : ?>
                        <a href="<?php Layout::partial('link'); ?>" class="item-link">
                        <?php endif; ?>
                            <?php Field::image('image', 'medium', ['class' => 'thumbnail']); ?>
                            <?php Field::html('label', '<p class="label">%s</p>'); ?>
                        <?php if (Field::anyExist('link_resource', 'link_url', 'link_anchor', 'link_file')) : ?>
                        </a>
                    <?php endif; ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
