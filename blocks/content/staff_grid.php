<?php

$members = new WP_Query([
    'post_type' => ['staff'],
    'posts_per_page' => -1,
    'tax_query' => [
        [
            'taxonomy' => 'groups',
            'field' => 'term_id',
            'terms' => Field::get('category'),
        ],
    ],
]);

if (Field::exists('category') || Field::exists('additional_staff')) :
    ?>
    <div class="staff-grid">
        <?php Layout::partial('title'); ?>

        <div class="staff-details">
            <?php while ($members->have_posts()) :
                $members->the_post();

                Field::setData(get_fields());

                ?>
                <div class="details-inner">
                    <div class="staff-info">
                        <div class="staff-name"><?php echo the_title(); ?></div>
                        <div class="staff-title"><?php Field::display('job_title'); ?></div>
                    </div>
                    <div class="staff-contact">
                        <div class="staff-email">
                            <a href="mailto:<?php Field::display('email_address'); ?>" class="email-link"><em class="far fa-at"></em></a>
                        </div>
                    </div>
                </div>
                <?php Field::restore(); ?>
            <?php endwhile; ?>

            <?php if (Field::exists('additional_staff')) : ?>
                <?php foreach (Field::relationship('additional_staff') as $loop) : ?>
                    <div class="details-inner">
                        <div class="staff-info">
                            <div class="staff-name"><?php echo the_title(); ?></div>
                            <div class="staff-title"><?php Field::display('job_title'); ?></div>
                        </div>
                        <div class="staff-contact">
                            <div class="staff-email">
                                <a href="mailto:<?php Field::display('email_address'); ?>" class="email-link"><em class="far fa-at"></em></a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
<?php wp_reset_query(); ?>
