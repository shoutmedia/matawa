<?php
    $staff_id = Field::get('content')->ID;
 ?>
<div class="staff-member">
    <?php if (has_post_thumbnail($staff_id)) : ?>
        <div class="staff-photo">
            <?php echo get_the_post_thumbnail($staff_id, 'full'); ?>
        </div>
    <?php endif; ?>
    <div class="staff-details">
        <?php Layout::partial('title'); ?>
        <div class="details-inner">
            <div>
                <div class="staff-name"><?php echo get_the_title($staff_id); ?></div>
                <div class="staff-jobtitle"><?php echo get_field('job_title', $staff_id); ?></div>
            </div>
            <div>
                <div class="staff-email">
                    E: <a href="mailto:<?php echo get_field('email_address', $staff_id); ?>" class="email-link"><?php echo get_field('email_address', $staff_id); ?></a>
                </div>
                <div class="staff-phone">
                    P: <a href="tel:<?php echo preg_replace('/[^0-9,.]/', '', get_field('phone_number', $staff_id)); ?>" class="phone-link"><?php echo get_field('phone_number', $staff_id); ?></a>
                </div>
            </div>
        </div>
    </div>
    <a class="btn-contact" href="mailto:<?php echo get_field('email_address', $staff_id); ?>">
        <?php Field::display('button_text'); ?>
    </a>
</div>
