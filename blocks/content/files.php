<?php

$exts = [
    'jpg'  => 'image',
    'jpeg' => 'image',
    'gif'  => 'image',
    'png'  => 'image',
    'pdf'  => 'pdf',
    'csv'  => 'excel',
    'xls'  => 'excel',
    'xlsx' => 'excel',
    'potx' => 'powerpoint',
    'docx' => 'word',
    'doc'  => 'word',
    'zip'  => 'archive',
    'rar'  => 'archive',
    'wmv'  => 'video',
    'mp4'  => 'video',
    'mov'  => 'video',
    'mp3'  => 'sound',
];

?>

<div class="files">
    <?php if (Field::exists('heading')) : ?>
        <h3 class="files-heading"><?php Field::display('heading'); ?></h3>
    <?php endif; ?>
    <div class="files-container">
        <?php foreach (Field::iterable('files') as $loop) : ?>
            <?php
                $file = Field::get('file');
                $ext = explode(".", $file['url']);
                $ext = strtolower($ext[count($ext) - 1]);
            ?>
            <a href="<?php echo $file['url']; ?>" class="file">
                <i class="fas fa-<?php echo array_key_exists($ext, $exts) ? 'file-' . $exts[$ext] : 'file'; ?>"></i>
                <span class="file-link">
                    <?php Field::display('label'); ?>
                </span>
            </a>
        <?php endforeach; ?>
    </div>
</div>
