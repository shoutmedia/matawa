<div class="accordion all-staff">
    <div class="accordion-content">
        <?php foreach (Field::iterable('groups') as $loop) : ?>
            <h3><?php Field::display('title'); ?></h3>
            <?php foreach (Field::get('categories') as $term) :
                $termDetails = get_term($term);

                $members = new WP_Query([
                    'post_type' => ['staff'],
                    'posts_per_page' => -1,
                    'tax_query' => [
                        [
                            'taxonomy' => 'groups',
                            'field' => 'term_id',
                            'terms' => $term,
                        ],
                    ],
                ]);

                ?>
                <div class="item-heading">
                    <span class="label"><?php echo $termDetails->name; ?></span>
                    <i class="fas fa-chevron-down"></i>
                </div>
                <div class="item-content item-hidden">
                    <div class="staff-grid">
                        <div class="staff-details">
                            <?php while ($members->have_posts()) :
                                $members->the_post();

                                Field::setData(get_fields());

                                ?>
                                <div class="details-inner">
                                    <div class="staff-info">
                                        <div class="staff-name"><?php echo the_title(); ?></div>
                                        <div class="staff-title"><?php Field::display('job_title'); ?></div>
                                    </div>
                                    <div class="staff-contact">
                                        <div class="staff-email">
                                            <a href="mailto:<?php Field::display('email_address'); ?>" class="email-link"><em class="far fa-at"></em></a>
                                        </div>
                                    </div>
                                </div>
                                <?php Field::restore(); ?>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>
                <?php wp_reset_query(); ?>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </div>
</div>
