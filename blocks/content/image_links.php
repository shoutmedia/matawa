<div class="image-links">
    <?php foreach (Field::iterable('links') as $loop) : ?>
        <div class="link" style="background-image: url(<?php echo Field::src('image'); ?>);">
            <a href="<?php Layout::partial('link'); ?>">
                <span class="label"><?php Field::display('label'); ?></span>
            </a>
        </div>
    <?php endforeach; ?>
</div>
