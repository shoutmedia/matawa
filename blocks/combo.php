<div class="<?php Layout::classes('combo'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partials('videobg', 'overlay'); ?>

    <?php if (Field::exists('class_contain_nested_backgrounds')) : ?>
        <div class="container">
    <?php endif; ?>

    <?php Layout::flexible(Field::get('blocks', [])); ?>

    <?php if (Field::exists('class_contain_nested_backgrounds')) : ?>
        </div>
    <?php endif; ?>
</div>
