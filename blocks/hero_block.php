<div class="<?php Layout::classes('hero-block'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <div class="inner">
            <?php if (Field::exists('title')) : ?>
                <div class="feature"><?php _e('Feature', DOMAIN); ?></div>
                <div class="title-block">
                    <?php Field::html('title', '<h2 class="title">%s</h2>'); ?>
                </div>
            <?php endif; ?>
            <?php Field::display('description'); ?>
            <?php Layout::partial('buttons'); ?>
        </div>
    </div>
</div>
