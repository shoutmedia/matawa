<div class="search-results">
    <div class="container">
        <h2 class="title">
            <?php _e('Search Results', DOMAIN); ?>
        </h2>
        <?php if (have_posts()) : ?>

            <?php while (have_posts()) :
                the_post(); ?>

                <article <?php post_class() ?> id="post-<?php the_ID(); ?>">
                    <div class="post-header">
                        <div class="post-title">
                            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                            <!-- <div class="post-type">
                                <?php echo get_post_type_object(get_post_type())->labels->singular_name; ?>
                            </div> -->
                        </div>
                        <p class="read-more"><a href="<?php the_permalink(); ?>"><strong><?php _e('Read More', DOMAIN); ?></strong>&nbsp;<i class="fas fa-caret-square-right" aria-hidden="true"></i></a></p>
                    </div>
                    <?php if (in_array(get_post_type(), ['post', 'event', 'jobpost'])) : ?>
                        <div class="excerpt"><?php the_excerpt(); ?></div>
                    <?php endif; ?>

                </article>

            <?php endwhile; ?>

        <?php else : ?>

            <h2><?php _e('Nothing Found', DOMAIN); ?></h2>

        <?php endif; ?>
    </div>
</div>
