<?php

$container = '';

if (Field::exists('accent_image')) :
    Layout::addClass('has-accent');
    $container = ' style="background-image: url(' . Field::src('accent_image', 'large') . ');"';
endif;

?>
<div class="<?php Layout::classes('single-column-flexible'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container"<?php echo $container; ?>>
        <div class="inner">
            <?php Layout::flexible(Field::get('components', []), 'blocks/components'); ?>
        </div>
    </div>
</div>
