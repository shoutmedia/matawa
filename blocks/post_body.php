<div class="content-with-sidebar side-right single-post">
    <div class="container">
        <div class="content">
            <div class="title-block">
                <h2 class="title"><?php the_title(); ?></h2>
            </div>
            <div class="post-meta">
                <div class="post-date"><?php the_date(); ?></div>
                <div class="post-author"><?php the_category(', '); ?></div>
            </div>
            <?php the_content(); ?>
        </div>
        <aside class="sidebar posts-sidebar">
            <div class="title-block">
                <h3 class="title"><?php _e('Recent News', DOMAIN); ?></h3>
            </div>
            <?php

            $articles = new WP_Query([
                'posts_per_page' => 8,
                'post__not_in' => [get_the_ID()],
            ]);

            if ($articles->have_posts()) :
                echo '<ul>';

                while ($articles->have_posts()) :
                    $articles->the_post();
                    echo '<li><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></li>';
                endwhile;

                echo '</ul>';
            endif;

            ?>
        </aside>
    </div>
</div>
