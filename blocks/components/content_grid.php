<div class="nested-content-grid">
    <?php foreach (Field::iterable('items') as $loop) : ?>
        <a href="<?php Layout::partial('link'); ?>" class="item" style="background-image: url(<?php echo Field::src('image', 'large'); ?>">
            <?php Field::html('label', '<p class="label">%s <em class="far fa-chevron-right"></em></p>'); ?>
        </a>
    <?php endforeach; ?>
</div>
