<?php

$bg = '';
$shared = Field::exists('shared_background');

if ($shared) :
    $bg = ' style="background-image: url(' . Field::src('background', 'banner') . ')"';
endif;

?>
<div class="slideshow"<?php echo $bg; ?>>
    <?php Field::html('fixed_title', '<p class="fixed-title">%s</p>'); ?>
    <div class="slides">
        <?php foreach (Field::iterable('slides') as $loop) : ?>
            <div class="slide"<?php echo $shared ? '' : ' style="' . Field::src('background_image', 'banner') . '"'; ?>>
                <div class="inner">
                    <?php Field::display('content'); ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
