<div class="icon-grid">
    <?php foreach (Field::iterable('items') as $loop) : ?>
        <div class="item">
            <div class="icon">
                <?php Field::image('icon', 'medium'); ?>
            </div>
            <?php Field::html('label', '<p class="label">%s</p>'); ?>
        </div>
    <?php endforeach; ?>
</div>
