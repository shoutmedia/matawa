<?php if (Field::exists('buttons')) : ?>
    <p class="<?php Layout::classes('buttons'); ?>">
        <?php

        foreach (Field::iterable('buttons') as $btn) :
            Layout::partial('button');
        endforeach;

        ?>
    </p>
<?php endif; ?>
