<div class="<?php Layout::classes('map'); ?>"<?php Layout::id(); ?>>
    <div class="acf-map">
        <?php foreach (Field::iterable('markers') as $loop) :

            $location = Field::get('marker');

            ?>
            <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
                <p class="address"><?php echo $location['address']; ?></p>
            </div>
        <?php endforeach; ?>
    </div>
</div>
