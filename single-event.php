<?php get_header(); ?>

    <main class="main-content" role="main" id="main" tabindex="-1">
        <?php if (have_posts()) :
            while (have_posts()) :
                the_post();

                Banner::render();

                global $EM_Event;
                echo $EM_Event->output(file_get_contents(get_template_directory() . '/template/single-event.php'));

                Layout::render(['default' => '']);
            endwhile;
        endif; ?>

    </main>

<?php get_footer(); ?>
