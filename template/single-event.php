<div class="container">
    <div class="em-single-event-page">
        <div class="event-left">
            <h2 class="em-event-title">#_EVENTNAME</h2>
            <div class="em-event-datetime">#_EVENTDATES&nbsp;&nbsp;>&nbsp;&nbsp;#_EVENTTIMES</div>
            {has_image}
            <div class="em-event-image">#_EVENTIMAGE</div>
            {/has_image}
            <div class="em-event-content">#_EVENTNOTES</div>
            {has_location}
            <div class="event-location">
                <div class="location-map">#_LOCATIONMAP</div>
                <div class="location-details">
                    <p>
                        #_LOCATIONLINK<br />
                        #_LOCATIONADDRESS<br />
                        #_LOCATIONTOWN, #_LOCATIONSTATE<br />
                        #_LOCATIONPOSTCODE
                    </p>
                </div>
            </div>
            {/has_location}
        </div>
        {has_bookings}
        <div class="event-right">
            <h3 class="booking-form-title">Register</h3>
            #_BOOKINGFORM
        </div>
        {/has_bookings}
    </div>
</div>
